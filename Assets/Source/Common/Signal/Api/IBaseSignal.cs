﻿using System;

namespace Common.Signal.Api
{
    public interface IBaseSignal
    {
        void Dispatch(object[] args);

        void AddListener(Action<IBaseSignal, object[]> callback);

        void AddOnce(Action<IBaseSignal, object[]> callback);

        void RemoveListener(Action<IBaseSignal, object[]> callback);

        void RemoveAllListeners();
    }
}
