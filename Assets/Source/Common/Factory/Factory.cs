﻿using UnityEngine;

namespace Common.Factory
{
    public abstract class Factory
    {
        public abstract T GetInstance<T>(params object[] args) where T : MonoBehaviour;
    }
}