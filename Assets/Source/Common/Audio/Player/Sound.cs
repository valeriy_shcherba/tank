﻿using System;
using UnityEngine;

namespace Common.Audio.Player
{
    [Serializable]
    public class Sound
    {
        public string Name;
        public AudioClip Clip;
    }
}
