﻿using UnityEngine;
using DG.Tweening;

namespace Common.Audio.Player
{
    public class SoundPlayer : MonoBehaviour
    {
        private AudioSource _audioSource;
        private float _defaultVolume;

        private const float FadeOutDuration = 0.5f;
        private const float FadeInDuration = 1f;

        public AudioSource AudioSource => _audioSource;

        public void Initialize()
        {
            _audioSource = GetComponent<AudioSource>();
            _defaultVolume = _audioSource.volume;
        }

        public void Release()
        {
            _audioSource = null;
        }

        public void SetLoop(bool isLoop)
        {
            _audioSource.loop = isLoop;
        }

        public void Play(Sound sound, bool isOneShot)
        {
            if (sound == null || sound.Clip == null) return;

            if (isOneShot)
            {
                _audioSource.PlayOneShot(sound.Clip);
            }
            else
            {
                if (!_audioSource.isPlaying)
                {
                    PlayClip(sound.Clip);

                    _audioSource.DOFade(_defaultVolume, FadeInDuration).Play();
                }
                else
                {
                    _audioSource.DOFade(0f, FadeOutDuration).OnComplete(delegate
                    {
                        PlayClip(sound.Clip);

                        _audioSource.DOFade(_defaultVolume, FadeInDuration).Play();
                    })
                    .Play();
                }
            }
        }

        private void PlayClip(AudioClip clip)
        {
            _audioSource.clip = clip;
            _audioSource.Play();
        }
    }
}
