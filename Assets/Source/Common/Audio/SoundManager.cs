﻿using System.Collections.Generic;
using Common.Audio.Player;
using UnityEngine;

namespace Common.Audio
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField]
        private List<SoundPlayer> _sfxPlayer;

        [SerializeField]
        private List<SoundPlayer> _musicPlayer;

        [SerializeField]
        private Sound[] _defaultSounds;

		private bool _isSfxMuted;
		private bool _isMusicMuted;

        private Dictionary<string, Sound> _sounds;

		public void Initialize()
		{
			_sounds = new Dictionary<string, Sound>();
		
		    _sfxPlayer.ForEach(x => x.Initialize());
		    _musicPlayer.ForEach(x => x.Initialize());

			ChangeMusicStatus(_isMusicMuted);
			ChangeSfxStatus(_isSfxMuted);

            AddDefaultSounds();
        }

        public void Release()
        {
            _sounds?.Clear();
            _sounds = null;
        }

        public void AddSound(Sound sound)
        {
            if (sound == null) return;

            _sounds.Add(sound.Clip.name, sound);
        }

        public void PlaySfx(string soundName)
        {
            SoundPlayer player = _sfxPlayer.Find(x => !x.AudioSource.isPlaying);
            if (player == null) return;
            
            Sound sound = TryGetSound(soundName);
            PlaySound(sound, player, true);
        }

		public void PlayMusic(string soundName)
        {
            SoundPlayer player = _musicPlayer.Find(x => !x.AudioSource.isPlaying);
            if (player == null) return;;
            
            Sound sound = TryGetSound(soundName);

            if (sound != null)
            {
                PlaySound(sound, player, false);
            }
        }

        public void PlaySound(string soundName, SoundPlayer soundPlayer)
        {
            Sound sound = TryGetSound(soundName);

            if (sound != null)
            {
                PlaySound(sound, soundPlayer, true);
            }
        }

        private void PlaySound(Sound sound, SoundPlayer soundPlayer, bool isOneShot)
        {
            soundPlayer.Play(sound, isOneShot);
        }

		public void ChangeSfxStatus(bool isMuted)
		{
			_isSfxMuted = isMuted;
		    _sfxPlayer.ForEach(x => x.AudioSource.volume = (_isSfxMuted) ? 0f : 1f);
		}

		public void ChangeMusicStatus(bool isMuted)
		{
			_isMusicMuted = isMuted;
			_musicPlayer.ForEach(x => x.AudioSource.volume = (_isMusicMuted) ? 0f : 1f);
		}

        private Sound TryGetSound(string soundName)
        {
			Sound sound;
            _sounds.TryGetValue(soundName, out sound);

            return sound;
        }

        private void AddDefaultSounds()
        {
            if (_defaultSounds == null) return;

            for (int i = 0, imax = _defaultSounds.Length; i < imax; ++i)
            {
                Sound defaultSound = _defaultSounds[i];
                AddSound(defaultSound);
            }
        }
    }
}
