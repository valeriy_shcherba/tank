﻿using System;
using System.Collections;
using Common.Singleton;
using UnityEngine;

namespace Common.Coroutine
{
	public class CoroutineRunner : UnitySingleton<CoroutineRunner>
	{
		public static UnityEngine.Coroutine Wait(float seconds, Action callback)
		{
			return Instance.StartCoroutine(WaitEnumerator(seconds, callback));
		}

		private static IEnumerator WaitEnumerator(float seconds, Action callback)
		{
			yield return new WaitForSeconds(seconds);

			callback();
		}
	}
}