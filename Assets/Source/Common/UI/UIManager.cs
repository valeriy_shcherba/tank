﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Tank.UI.Popups;
using Tank.UI.Windows;

namespace Common.UI
{
	public class UIManager
	{
		private Dictionary<Type, string> _popupsMap;
		private Dictionary<Type, string> _windowsMap;
		
		private UIPopupController _currentPopup;
		private UIPopupController _previousPopup;

		private UIWindowController _currentWindow;

		private Transform _popupsContent;
		private Transform _windowsContent;
		
		public void Initialize()
		{
			_popupsMap = new Dictionary<Type, string>();
			_windowsMap = new Dictionary<Type, string>();
			
			Transform uiTransform = new GameObject("[UI]").transform;
			
			_popupsContent = new GameObject("[Popups]").transform;
			_windowsContent = new GameObject("[Windows]").transform;
			
			_popupsContent.SetParent(uiTransform, false);
			_windowsContent.SetParent(uiTransform, false);
			
			BindPopup<StartPopupController>("UI/Popup - Start");
			BindWindow<GameWindowController>("UI/Window - Game");
		}

		public void ShowWindow<TWindowController> ()
		{
			string windowPath = null;
			_windowsMap.TryGetValue(typeof(TWindowController), out windowPath);
			
			if (string.IsNullOrEmpty(windowPath)) return;
			
			if (_currentWindow != null)
			{
				_currentWindow.Hide();
				MonoBehaviour.Destroy(_currentWindow.gameObject);
				_currentWindow = null;
			}
			
			UIWindowController window = MonoBehaviour.Instantiate(Resources.Load<UIWindowController>(windowPath), _windowsContent);
			
			window.Show();
			
			_currentWindow = window;
		}
			
		public void ShowPopup<TPopupController> () where TPopupController : UIPopupController
		{
			string popupPath = null;
			_popupsMap.TryGetValue(typeof(TPopupController), out popupPath);

			if (string.IsNullOrEmpty(popupPath)) return;
			
			if (_currentPopup != null)
			{
				HidePopup();
			}

			UIPopupController popup = MonoBehaviour.Instantiate(Resources.Load<UIPopupController>(popupPath), _popupsContent);
			popup.Show();
			
			_previousPopup = _currentPopup;
			_currentPopup = popup;
		}

		public void HidePopup()
		{
			if (_currentPopup != null)
			{
				_currentPopup.Hide();
				MonoBehaviour.Destroy(_currentPopup.gameObject);
				_currentPopup = null;
			}
			
			if (_previousPopup == null) return;

			_currentPopup = _previousPopup;
			_previousPopup = null;
		}
		
		private void BindPopup<TPopupController>(string path)
		{
			_popupsMap.Add(typeof(TPopupController), path);
		}
		
		private void BindWindow<TPopupController>(string path)
		{
			_windowsMap.Add(typeof(TPopupController), path);
		}
	}
}