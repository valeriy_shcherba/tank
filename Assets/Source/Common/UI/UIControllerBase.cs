﻿using UnityEngine;

namespace Common.UI
{
	public class UIControllerBase : MonoBehaviour
	{
		public void Show()
		{
			Initialize();
		}

		public void Hide()
		{
			Release();
		}
        
		protected virtual void Initialize()
		{
            
		}

		protected virtual void Release()
		{
            
		}
	}
}