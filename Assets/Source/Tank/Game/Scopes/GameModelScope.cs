﻿using Tank.Game.Models;

namespace Tank.Game.Scopes
{
	public class GameModelScope
	{
		public HeartbeatRegistryModel HeartbeatRegistryModel { get; }
		public GameModel GameModel { get; }
		public TaskRegistryModel TaskModel { get; }
		public PoolRegistryModel PoolModel { get; }
		public EnemyRegistryModel EnemyModel { get; }
		
		public GameModelScope()
		{
			HeartbeatRegistryModel = new HeartbeatRegistryModel();
			GameModel = new GameModel();
			TaskModel = new TaskRegistryModel();
			PoolModel = new PoolRegistryModel();
			EnemyModel = new EnemyRegistryModel();
		}
	}
}