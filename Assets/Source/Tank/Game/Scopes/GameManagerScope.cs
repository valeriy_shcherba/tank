﻿using Tank.Game.Context;
using Tank.Game.Managers.Heartbeat;
using Tank.Game.Managers.Input;
using Tank.Game.Managers.Pool;
using Tank.Game.Managers.Task;

namespace Tank.Game.Scopes
{
	public class GameManagerScope
	{
		public PoolManager PoolManager { get; }
		public HeartbeatManager HeartbeatManager { get; }
		public TaskManager TaskManager { get; }
		public InputManager InputManager { get; }
		
		public GameManagerScope(GameContext context)
		{
			PoolManager = new PoolManager(context);
			HeartbeatManager = new HeartbeatManager(context);
			TaskManager = new TaskManager(context);
			InputManager = new InputManager(context);
		}
	}
}