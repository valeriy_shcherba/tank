﻿using Tank.Game.Context;
using Tank.Game.Controllers.Commands;

namespace Tank.Game.Scopes
{
    public class GameCommandScope
    {
        //Setup
        public InitializeGameCommand InitializeCommand { get; }
        public StartGameCommand StartCommand { get; }
        public StopGameCommand StopCommand { get; }

        //Loop
        public EnemyDamageMadeCommand EnemyDamageMadeCommand { get; }
        public TankShootBulletCommand TankShootBulletCommand { get; }
        public TankDamageMadeCommand TankDamageMadeCommand { get; }
        public StartRocketCommand StartRocketCommand { get; }
        
        public GameCommandScope(GameContext context)
        {
            InitializeCommand = new InitializeGameCommand(context);
            StartCommand = new StartGameCommand(context);
            StopCommand = new StopGameCommand(context);

            EnemyDamageMadeCommand = new EnemyDamageMadeCommand(context);
            TankShootBulletCommand = new TankShootBulletCommand(context);
            TankDamageMadeCommand = new TankDamageMadeCommand(context);
            StartRocketCommand = new StartRocketCommand(context);
        }
    }
}