﻿using System.Collections.Generic;
using Common.Signal.Impl;
using Tank.Game.Actors.Base;
using Tank.Game.Data.Types;

namespace Tank.Game.Scopes
{
	public class GameSignalScope
	{
		//Events
		public Signal GameInitializedSignal { get; }
		
		public Signal GameFinishedSignal { get; }
		
		public Signal<InputType> InputButtonPressedSignal { get; }
		
		//Actions
		public Signal<WeaponType> TankDamageMadeSignal { get; }
		public Signal<List<EnemyActor>> BulletExplodedSignal { get; }
		public Signal<int> EnemyDamageMadeSignal { get; }
		
		public GameSignalScope()
		{
			GameInitializedSignal = new Signal();
			GameFinishedSignal = new Signal();
			InputButtonPressedSignal = new Signal<InputType>();
			
			TankDamageMadeSignal = new Signal<WeaponType>();
			BulletExplodedSignal = new Signal<List<EnemyActor>>();
			EnemyDamageMadeSignal = new Signal<int>();
		}
	}
}