﻿namespace Tank.Game.Data.Types
{
	public enum WeaponType
	{
		NONE = 0,
		TYPE_A = 1,
		TYPE_B = 2
	}
}