﻿namespace Tank.Game.Data.Types
{
	public enum EnemyType
	{
		NONE = 0,
		
		ALIEN = 1,
		CHIBIMUMMY = 2,
		DRONE = 3
	}
}