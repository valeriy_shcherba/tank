﻿namespace Tank.Game.Data.Types
{
	public enum InputType
	{
		NONE = 0,
		MOVE_FORWARD = 1,
		MOVE_BACK = 2,
		MOVE_RIGHT = 3,
		MOVE_LEFT = 4,
		CHANGE_WEAPON_A = 5,
		CHANGE_WEAPON_B = 6,
		ATTACK = 7
	}
}