﻿namespace Tank.Game.Data.Types
{
	public enum ProjectTileType
	{
		NONE = 0,
		
		TANK_BULLET_A = 1,
		TANK_BULLET_B = 2,
		ROCKET = 3
	}
}
