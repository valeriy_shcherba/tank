﻿namespace Tank.Game.Data.Types
{
	public enum TaskState 
	{
		NONE,
		STARTED,
		RUNNING,
		STOPPED,
		COMPLETED
	}
}
