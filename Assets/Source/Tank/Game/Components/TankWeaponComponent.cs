﻿using UnityEngine;

namespace Tank.Game.Components
{
	public class TankWeaponComponent : MonoBehaviour
	{
		[SerializeField] private Transform _spawnPoint;
		
		public Transform SpawnPoint => _spawnPoint;
			
		public void ShowWeapon()
		{
			gameObject.SetActive(true);
		}

		public void HideWeapon()
		{
			gameObject.SetActive(false);
		}
	}
}