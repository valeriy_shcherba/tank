﻿using System.Collections.Generic;
using Tank.Game.Data.Types;
using UnityEngine;

namespace Tank.Game.Factory
{
	public class EnemyFactory : Common.Factory.Factory
	{
		private readonly Dictionary<EnemyType, string> _prefabMap = new Dictionary<EnemyType, string>()
		{
			{EnemyType.CHIBIMUMMY, "Game/Enemies/ChibiMummy"},
			{EnemyType.ALIEN, "Game/Enemies/Alien"},
			{EnemyType.DRONE, "Game/Enemies/Drone"},
		};

		public override T GetInstance<T>(params object[] args)
		{
			EnemyType type = (EnemyType) args[0];
			string path = _prefabMap[type];
			
			return MonoBehaviour.Instantiate(Resources.Load<T>(path));
		}
	}
}
