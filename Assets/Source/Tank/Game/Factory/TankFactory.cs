﻿using System.Collections.Generic;
using Tank.Game.Data.Types;
using UnityEngine;

namespace Tank.Game.Factory
{
	public class TankFactory : Common.Factory.Factory
	{
		private readonly Dictionary<TankType, string> _prefabMap = new Dictionary<TankType, string>()
		{
			{TankType.TANK, "Game/Tanks/Tank"}
		};

		public override T GetInstance<T>(params object[] args)
		{
			TankType type = (TankType) args[0];

			string path = _prefabMap[type];
			return MonoBehaviour.Instantiate(Resources.Load<T>(path));
		}
	}
}
