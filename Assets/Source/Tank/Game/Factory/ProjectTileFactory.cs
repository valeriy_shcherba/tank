﻿using System.Collections.Generic;
using Tank.Game.Data.Types;
using UnityEngine;

namespace Tank.Game.Factory
{
	public class ProjectTileFactory : Common.Factory.Factory
	{
		private readonly Dictionary<ProjectTileType, string> _prefabMap = new Dictionary<ProjectTileType, string>()
		{
			{ProjectTileType.TANK_BULLET_A, "Game/ProjectTiles/RedTankBullet"},
			{ProjectTileType.TANK_BULLET_B, "Game/ProjectTiles/BlueTankBullet"},
			{ProjectTileType.ROCKET, "Game/ProjectTiles/Rocket"}
		};

		public override T GetInstance<T>(params object[] args)
		{
			ProjectTileType type = (ProjectTileType) args[0];
			string path = _prefabMap[type];
			
			return MonoBehaviour.Instantiate(Resources.Load<T>(path));
		}
	}
}

