﻿using Tank.Game.Context;

namespace Tank.Game.Managers.Heartbeat
{
	public class HeartbeatManager : ContextInject
	{
		public float Time;

		private float _delay;
		private float _startDelayTime;
		
		private bool _isStarted;
		private bool _isPaused;
		
		public HeartbeatManager(GameContext context) : base(context) {}

		public void Update()
		{
			if (!_isStarted || _isPaused) return;

			Time = UnityEngine.Time.time - _delay;
			
			var entitiesArray = HeartbeatRegistryModel.GetAllRegisteredEntities();

			for (int index = 0; index < entitiesArray.Length; index++)
			{
				entitiesArray[index].UpdateEntity();
			}
		}

		public void SetStart(bool isStarted)
		{
			_isStarted = isStarted;
		}

		public void SetPause(bool isPaused)
		{
			if (isPaused)
			{
				_startDelayTime = UnityEngine.Time.time;
			}

			if (!isPaused)
			{
				_delay += UnityEngine.Time.time - _startDelayTime;
			}
			
			_isPaused = isPaused;
		}
		
		public void RegisterEntity(IUpdatableEntity entity)
		{
			HeartbeatRegistryModel.RegisterEntity(entity);
		}

		public void UnregisterEntity(IUpdatableEntity entity)
		{
			HeartbeatRegistryModel.UnregisterEntity(entity);
		}
	}
}