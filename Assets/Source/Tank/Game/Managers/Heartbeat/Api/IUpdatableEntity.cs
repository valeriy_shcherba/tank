﻿namespace Tank.Game.Managers.Heartbeat
{
    public interface IUpdatableEntity
    {
        void UpdateEntity();
    }
}

