﻿using System;

namespace Tank.Game.Managers.Pool
{
	public interface IPoolableEntity
	{
		event Action OnSpawned;
		event Action<Poolable> OnDespawned;

		void Spawn();
		void Despawn();
	}
}