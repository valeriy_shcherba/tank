﻿using System.Collections.Generic;
using Tank.Game.Actors.Base;
using Tank.Game.Context;
using Tank.Game.Data.Types;
using Tank.Game.Factory;
using Tank.Game.Root;
using UnityEngine;

namespace Tank.Game.Managers.Pool
{
	public class PoolManager : ContextInject
	{
		private Dictionary<EnemyType, Pool> _enemyPoolsMap;
		private Dictionary<ProjectTileType, Pool> _projectTilePoolsMap;
		
		private Transform _field;
		
		public PoolManager(GameContext context) : base(context)
		{
			Bind();
		}

		private void Bind()
		{
			Transform root = new GameObject("[POOLS]").transform;
			_field = GameRoot.Instance.FieldTransform;
			
			EnemyFactory enemyFactory = new EnemyFactory();
			ProjectTileFactory projectTileFactory = new ProjectTileFactory();
			
			_enemyPoolsMap = new Dictionary<EnemyType, Pool>();
			_enemyPoolsMap.Add(EnemyType.CHIBIMUMMY, AddPool("CHIBIMUMMY", root, enemyFactory));
			_enemyPoolsMap.Add(EnemyType.ALIEN, AddPool("ALIEN", root, enemyFactory));
			_enemyPoolsMap.Add(EnemyType.DRONE, AddPool("DRONE", root, enemyFactory));

			_projectTilePoolsMap = new Dictionary<ProjectTileType, Pool>();
			_projectTilePoolsMap.Add(ProjectTileType.TANK_BULLET_A, AddPool("TANK_BULLET_A", root, projectTileFactory));
			_projectTilePoolsMap.Add(ProjectTileType.TANK_BULLET_B, AddPool("TANK_BULLET_B", root, projectTileFactory));
			_projectTilePoolsMap.Add(ProjectTileType.ROCKET, AddPool("ROCKET", root, projectTileFactory));
		}

		public T SpawnEnemy<T>(EnemyType type) where T : GameActor
		{
			return Spawn<T>(_enemyPoolsMap[type], type);
		}
		
		public T SpawnProjectTile<T>(ProjectTileType type) where T : GameActor
		{
			return Spawn<T>(_projectTilePoolsMap[type], type);
		}
		
		private Pool AddPool(string name, Transform root, Common.Factory.Factory factory)
		{
			var poolObject = new GameObject(name).transform;
			poolObject.SetParent(root, false);
			
			var poolEntity = new Pool(poolObject, factory);
			poolEntity.OnSpawned += OnEntitySpawned;
			poolEntity.OnDespawned += OnEntityDespawned;
			
			return poolEntity;
		}

		private T Spawn<T>(Pool pool, object arg) where T : GameActor
		{
			var entity = pool.Spawn<T>(arg);
			entity.transform.SetParent(_field, false);
			
			return entity;
		}

		private void OnEntitySpawned(Poolable entity)
		{
			_heartbeatManager.RegisterEntity((GameActor)entity);
		}
		
		private void OnEntityDespawned(Poolable entity)
		{
			_heartbeatManager.UnregisterEntity((GameActor)entity);
		}
	}
}