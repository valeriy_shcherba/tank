﻿using System;
using UnityEngine;

namespace Tank.Game.Managers.Pool
{
	public class Poolable : MonoBehaviour, IPoolableEntity
	{
		public event Action OnSpawned;
		public event Action<Poolable> OnDespawned;
		
		public void Spawn()
		{
			gameObject.SetActive(true);
			
			OnInitialize();
			
			OnSpawned?.Invoke();
		}

		public void Despawn()
		{
			OnRelease();
			
			gameObject.SetActive(false);
            
			OnDespawned?.Invoke(this);
		}

		protected virtual void OnInitialize() { }
		protected virtual void OnRelease() { }
	}
}