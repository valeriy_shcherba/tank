﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tank.Game.Managers.Pool
{
	public class Pool
	{
		private Stack<Poolable> _cachedObjects;

		private Common.Factory.Factory _factory;
		private Transform _pool;

		public event Action<Poolable> OnSpawned;
		public event Action<Poolable> OnDespawned;
		
		public Pool(Transform pool, Common.Factory.Factory factory)
		{
			_factory = factory;
			_pool = pool;
			
			_cachedObjects = new Stack<Poolable>();
		}
		
		public T Spawn<T>(params object[] args) where T : Poolable
		{
			var entity = _cachedObjects.Any() ? _cachedObjects.Pop() : _factory.GetInstance<T>(args);
			entity.OnDespawned += OnEntityDespawned;
			entity.Spawn();
			
			OnSpawned?.Invoke(entity);
			
			return (T)entity;
		}

		private void OnEntityDespawned(Poolable entity)
		{
			entity.OnDespawned -= OnEntityDespawned;
			entity.transform.SetParent(_pool);
			entity.transform.position = Vector3.zero;
			entity.transform.eulerAngles = Vector3.zero;
			
			_cachedObjects.Push(entity);
			
			OnDespawned?.Invoke(entity);
		}
	}
}