﻿using Tank.Game.Context;
using Tank.Game.Data.Types;
using Tank.Game.Managers.Heartbeat;
using UnityEngine;

namespace Tank.Game.Managers.Input
{
    public class InputManager: ContextInject, IUpdatableEntity
    {
        public InputManager(GameContext context) : base(context)
        {
            HeartbeatRegistryModel.RegisterEntity(this);
        }

        public void UpdateEntity()
        {
            if (UnityEngine.Input.GetKey(KeyCode.UpArrow))
            {
                _inputButtonPressedSignal.Dispatch(InputType.MOVE_FORWARD);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.DownArrow))
            {
                _inputButtonPressedSignal.Dispatch(InputType.MOVE_BACK);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.RightArrow))
            {
                _inputButtonPressedSignal.Dispatch(InputType.MOVE_RIGHT);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.LeftArrow))
            {
                _inputButtonPressedSignal.Dispatch(InputType.MOVE_LEFT);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.Q))
            {
                _inputButtonPressedSignal.Dispatch(InputType.CHANGE_WEAPON_A);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.W))
            {
                _inputButtonPressedSignal.Dispatch(InputType.CHANGE_WEAPON_B);
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.X))
            {
                _inputButtonPressedSignal.Dispatch(InputType.ATTACK);
            }
        }
    }
}

