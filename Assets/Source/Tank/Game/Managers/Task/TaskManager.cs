﻿using Tank.Game.Context;
using Tank.Game.Controllers.Tasks;
using Tank.Game.Managers.Heartbeat;

namespace Tank.Game.Managers.Task
{
	public class TaskManager : ContextInject, IUpdatableEntity
	{
		public TaskManager(GameContext context) : base(context)
		{
			HeartbeatRegistryModel.RegisterEntity(this);
		}

		public void AddTask(TaskBase task)
		{
			_taskModel.RegisterTask(task);
		}
		
		public void RemoveTask(TaskBase task)
		{
			_taskModel.UnregisterTask(task);
		}

		public void UpdateEntity()
		{
			var entities = _taskModel.GetAllRegistered();

			for (int index = 0; index < entities.Length; index++)
			{
				entities[index].UpdateEntity();
			}
		}
	}
}