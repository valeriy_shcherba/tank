﻿using Common.Singleton;
using Tank.Game.Context;
using Tank.Game.Proxy;
using UnityEngine;

namespace Tank.Game.Root
{
    public class GameRoot : UnitySingleton<GameRoot>
    {
        [SerializeField] private Transform _gameField;
        
        public Transform FieldTransform => _gameField;
        public GameProxy Proxy => _context.Proxy;
        
        private GameContext _context;

        private bool _isInitialized;
        
        public void Initialize()
        {
            _context = new GameContext();

            Proxy.InitializeGame();
            
            _isInitialized = true;
        }

        private void Release()
        {
            _isInitialized = false;
            
            _context.Release();
        }

        private void OnDestroy()
        {
            Release();
        }

        private void Update()
        {
            if (!_isInitialized) return;
            
            _context.ManagerScope.HeartbeatManager.Update();
        }
    }
}