﻿using Tank.Game.Managers.Heartbeat;
using Tank.Game.Managers.Pool;
using Tank.Game.Root;
using Tank.Game.Scopes;

namespace Tank.Game.Actors.Base
{
    public class GameActor : Poolable, IUpdatableEntity
    {
        protected bool _isAppeared;
        
        protected GameSignalScope _signalScope;
        
        public void UpdateEntity()
        {
            if (!_isAppeared) return;

            OnPhysics();
            OnUpdate();
        }

        protected virtual void OnUpdate()
        {
 
        }

        protected override void OnInitialize()
        {
            _signalScope = GameRoot.Instance.Proxy.SignalScope;
        }

        protected override void OnRelease()
        {
            _signalScope = null;
            _isAppeared = false;
        }

        protected virtual void OnPhysics() {}
    }
}

