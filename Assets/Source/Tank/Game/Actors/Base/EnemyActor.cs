﻿using Tank.Game.Data.Constants;
using Tank.Game.Domain;
using Tank.Game.Root;
using Tank.Root;
using UnityEngine;

namespace Tank.Game.Actors.Base
{
	public class EnemyActor : GameActor
	{
		[SerializeField] private Animator _enemyAnimator;
		
		private const string TRIGGER_DEAD = "DIE";
		
		private EnemyData _data;
		private Transform _target;
		
		private int _speed;
		
		private float _currentHP;
	
		private bool _isHitted;
		
		public void Setup(EnemyData data, Transform target)
		{
			_data = data;
			_target = target;

			_currentHP = _data.HP;
		}

		public virtual void SetDamage(int damage)
		{
			_currentHP -= damage - (damage * _data.Armor);

			if (_currentHP > 0) return;

			Disappeare();
		}
		
		protected override void OnUpdate()
		{
			base.OnUpdate();

			if (_isHitted) return;
			
			_speed += _speed == _data.MaxSpeed ? 0 : _data.Acceleration;
			
			transform.Translate(Vector3.forward * Time.deltaTime * _speed);
			transform.LookAt(_target);
			
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 25, LayerMask.NameToLayer("Tank")))
			{
				_isHitted = true;
				gameObject.layer = LayerMask.NameToLayer("NonCollision");
				_speed = 0;
				
				SetDamage(_data.Damage);
				GameRoot.Instance.Proxy.SignalScope.EnemyDamageMadeSignal.Dispatch(_data.Damage);
			}
		}

		protected virtual void OnActorAppeared()
		{
			AppRoot.Instance.SoundManager.PlaySfx(SoundConstants.SFX_EnemySpawn);
			_isAppeared = true;
		}

		protected virtual void OnActorDisappered()
		{	
			Despawn();
		}

		protected override void OnRelease()
		{
			base.OnRelease();
			
			_isHitted = false;
			gameObject.layer = LayerMask.NameToLayer("Enemy");
		}

		private void Disappeare()
		{
			_isAppeared = false;
			_enemyAnimator.SetTrigger(TRIGGER_DEAD);
			
			AppRoot.Instance.SoundManager.PlaySfx(SoundConstants.SFX_EnemyDespawn);
		}
	}
}