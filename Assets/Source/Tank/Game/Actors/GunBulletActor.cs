﻿using System.Collections.Generic;
using Common.Coroutine;
using Tank.Game.Actors.Base;
using Tank.Game.Root;
using UnityEngine;

namespace Tank.Game.Actors
{
	public class GunBulletActor : GameActor
	{
		[SerializeField] private GameObject _explosionEffect;

		private bool _isHitted;
		
		protected override void OnInitialize()
		{
			base.OnInitialize();

			_isAppeared = true;
		}

		protected override void OnPhysics()
		{
			if (_isHitted) return;
			
			transform.Translate(Vector3.forward * 150f * Time.deltaTime);
			transform.Rotate(Vector3.right * 10f * Time.deltaTime);
			
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 10))
			{
				OnRayHitted();
			}
		}

		private void OnRayHitted()
		{
			_isHitted = true;
			_explosionEffect.SetActive(true);
			
			CoroutineRunner.Wait(1f, ExplosionEffectEnded);
			
			Ray ray = new Ray(transform.position, Vector3.forward);
			RaycastHit[] entity = Physics.SphereCastAll(ray, 25f, LayerMask.NameToLayer("Enemy"));

			if (entity.Length <= 0) return;
			
			List<EnemyActor> enemies = new List<EnemyActor>();

			for (int index = 0; index < entity.Length; index++)
			{
				EnemyActor actor = entity[index].collider.GetComponent<EnemyActor>();
				if (actor == null) continue;
				enemies.Add(actor);
			}
		
			GameRoot.Instance.Proxy.SignalScope.BulletExplodedSignal.Dispatch(enemies);
		}

		private void ExplosionEffectEnded()
		{
			_explosionEffect.SetActive(false);
			_isHitted = false;
			
			Despawn();	
		}
	}
}