﻿using Common.Coroutine;
using Tank.Game.Actors.Base;
using Tank.Game.Components;
using Tank.Game.Data.Types;
using Tank.Game.Domain;
using UnityEngine;

namespace Tank.Game.Actors
{
	public class TankActor : GameActor
	{
		[SerializeField] protected Transform _hpBar;
		[SerializeField] private TankWeaponComponent[] _weapons;
	
		public TankWeaponComponent CurrentWeapon => _currentWeapon;
		public int Damage => _weaponSettings.Damage;
		
		private TankWeaponComponent _currentWeapon;
		private TankData _tankSettings;
		private WeaponData _weaponSettings;
		
		private float _movementSpeed = 0f;
		private float _currentHP;
		
		private bool _isWeaponBusy;

		public void Setup(TankData tankSettings)
		{
			_tankSettings = tankSettings;

			_currentHP = _tankSettings.HP;
				
			_currentWeapon = _weapons[0];
			_currentWeapon.ShowWeapon();
			
			_weaponSettings = _tankSettings.Weapons[0];
			_movementSpeed = 0f;
			
			_isAppeared = true;

			UpdateHP(_currentHP, _tankSettings.HP, out _currentHP);
		}

		public void SetDamage(int damage)
		{
			_currentHP -= damage - (damage * _tankSettings.Armor);

			UpdateHP(_currentHP, _tankSettings.HP, out _currentHP);
			
			if (_currentHP <= 0)
			{
				_signalScope.GameFinishedSignal.Dispatch();
			}
		}
		
		protected override void OnInitialize()
		{
			base.OnInitialize();

			_signalScope.InputButtonPressedSignal.AddListener(SignalScope_InputButtonPressed);
		}

		protected override void OnRelease()
		{
			_signalScope.InputButtonPressedSignal.RemoveListener(SignalScope_InputButtonPressed);

			base.OnRelease();
		}

		private void UpdateHP(float hp, int max, out float update)
		{
			hp = Mathf.Clamp(hp, 0f, max);
			_hpBar.localScale = new Vector3(hp / max, 1, 1);

			update = hp;
		}
	
		private void SignalScope_InputButtonPressed(InputType type)
		{
			if (type == InputType.MOVE_FORWARD)
			{
				_movementSpeed += _tankSettings.AccelerationOfMovement;

				Mathf.Clamp(_movementSpeed, _tankSettings.MaxSpeedBack, _tankSettings.MaxSpeedForward);
			}

			if (type == InputType.MOVE_BACK)
			{
				_movementSpeed -= _tankSettings.AccelerationOfMovement;

				Mathf.Clamp(_movementSpeed, _tankSettings.MaxSpeedBack, _tankSettings.MaxSpeedForward);
			}

			if (type == InputType.MOVE_LEFT)
			{
				transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * -_tankSettings.RotationSpeed);
			}

			if (type == InputType.MOVE_RIGHT)
			{
				transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * _tankSettings.RotationSpeed);
			}

			if (type == InputType.ATTACK)
			{
				Attack();
			}

			if (type == InputType.CHANGE_WEAPON_A)
			{
				ChangeWeapon(type);
			}

			if (type == InputType.CHANGE_WEAPON_B)
			{
				ChangeWeapon(type);
			}
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();

			Ray ray = new Ray(transform.position, -transform.up);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 25, LayerMask.NameToLayer("Default")))
			{
				if (hit.collider == null)
				{
					SetDamage(1000);
				}
			}
			
			if (Damp()) return;

			transform.position += transform.forward * Time.deltaTime * _movementSpeed;
		}

		private bool Damp()
		{
			if (_movementSpeed > 0)
			{
				_movementSpeed -= _tankSettings.AccelerationOfDamping;

				if (_movementSpeed < 0)
				{
					_movementSpeed = 0;
				}
			}
			else if (_movementSpeed < 0)
			{
				_movementSpeed += _tankSettings.AccelerationOfDamping;

				if (_movementSpeed > 0)
				{
					_movementSpeed = 0;
				}
			}

			return _movementSpeed == 0;
		}

		private void Attack()
		{
			if (_isWeaponBusy) return;
			
			_isWeaponBusy = true;

			CoroutineRunner.Wait(_weaponSettings.AttackSpeed, () => { _isWeaponBusy = false; });
			
			_signalScope.TankDamageMadeSignal.Dispatch(_weaponSettings.Type);
		}
		

		private void ChangeWeapon(InputType type)
		{
			int index = type == InputType.CHANGE_WEAPON_A ? 0 : 1;
		
			TankWeaponComponent tempWeapon = _weapons[index];
			
			if (_currentWeapon == tempWeapon) return;
			
			_currentWeapon.HideWeapon();
			_currentWeapon = tempWeapon;
			_currentWeapon.ShowWeapon();

			_weaponSettings = _tankSettings.Weapons[index];
		}
	}
}