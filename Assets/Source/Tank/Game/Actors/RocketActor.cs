﻿using Common.Coroutine;
using Tank.Game.Actors.Base;
using UnityEngine;

namespace Tank.Game.Actors
{
	public class RocketActor : GameActor
	{
		[SerializeField] private GameObject _explosionEffect;
		
		private EnemyActor _target;
		
		public void Setup(EnemyActor target)
		{
			_target = target;
			_isAppeared = true;
		}

		protected override void OnPhysics()
		{
			if (_target == null)
			{
				StartExplosion();
				return;
			}
			
			transform.Translate(Vector3.forward * 150f * Time.deltaTime);
			transform.LookAt(_target.transform.position);
			
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 10))
			{
				StartExplosion();
			}
		}

		private void StartExplosion()
		{
			_explosionEffect.SetActive(true);
			CoroutineRunner.Wait(1f, OnExplosionEnded);
		}
		
		private void OnExplosionEnded()
		{
			_target.Despawn();
			Despawn();	
		}
	}
}