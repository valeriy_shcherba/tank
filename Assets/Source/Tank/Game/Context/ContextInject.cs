﻿using System.Collections.Generic;
using Common.Signal.Impl;
using Tank.Game.Actors.Base;
using Tank.Game.Controllers.Commands;
using Tank.Game.Core;
using Tank.Game.Data.Types;
using Tank.Game.Managers.Heartbeat;
using Tank.Game.Managers.Input;
using Tank.Game.Managers.Pool;
using Tank.Game.Managers.Task;
using Tank.Game.Models;

namespace Tank.Game.Context
{
	public class ContextInject
	{
		protected GameContext _context;

		public ContextInject(GameContext context)
		{
			_context = context;
		}
		
		//Commands
		protected InitializeGameCommand _initializeGameCommand => _context.CommandScope.InitializeCommand;
		protected StartGameCommand _startGameCommand => _context.CommandScope.StartCommand;
		protected StopGameCommand _stopGameCommand => _context.CommandScope.StopCommand;

		protected EnemyDamageMadeCommand _enemyDamageMadeCommand => _context.CommandScope.EnemyDamageMadeCommand;
		protected TankShootBulletCommand _tankShootBulletCommand => _context.CommandScope.TankShootBulletCommand;
		protected TankDamageMadeCommand _tankDamageMadeCommand => _context.CommandScope.TankDamageMadeCommand;
		protected StartRocketCommand _startRocketCommand => _context.CommandScope.StartRocketCommand;
		
		//Managers
		protected PoolManager _poolManager => _context.ManagerScope.PoolManager;
		protected HeartbeatManager _heartbeatManager => _context.ManagerScope.HeartbeatManager;
		protected TaskManager _taskManager => _context.ManagerScope.TaskManager;
		protected InputManager _inputManager => _context.ManagerScope.InputManager;
		
		//Models
		protected HeartbeatRegistryModel HeartbeatRegistryModel => _context.ModelScope.HeartbeatRegistryModel;
		protected GameModel _gameModel => _context.ModelScope.GameModel;
		protected TaskRegistryModel _taskModel => _context.ModelScope.TaskModel;
		protected PoolRegistryModel _poolModel => _context.ModelScope.PoolModel;
		protected EnemyRegistryModel _enemyModel => _context.ModelScope.EnemyModel;
		
		//Signals
		protected Signal _gameInitialized => _context.SignalScope.GameInitializedSignal;
		protected Signal _gameFinishedSignal => _context.SignalScope.GameFinishedSignal;
		protected Signal<InputType> _inputButtonPressedSignal => _context.SignalScope.InputButtonPressedSignal;

		protected Signal<WeaponType> _tankDamageMadeSignal => _context.SignalScope.TankDamageMadeSignal;
		protected Signal<List<EnemyActor>> _bulletExplodedSignal => _context.SignalScope.BulletExplodedSignal;
		protected Signal<int> _enemyDamageMadeSignal => _context.SignalScope.EnemyDamageMadeSignal;
		
		//Configurator
		protected CoreConfigurator _configurator => _context.Configurator;
	}
}