﻿using Tank.Game.Core;
using Tank.Game.Mediators;
using Tank.Game.Proxy;
using Tank.Game.Scopes;

namespace Tank.Game.Context
{
	public class GameContext
	{
		public GameProxy Proxy { get; }
		
		public GameCommandScope CommandScope { get; }
		public GameManagerScope ManagerScope { get; }
		public GameModelScope ModelScope { get; }
		public GameSignalScope SignalScope { get; }
		
		public CoreConfigurator Configurator { get; }
		
		private GameActionMediator ActionMediator { get; }
		
		public GameContext()
		{
			Proxy = new GameProxy(this);
			
			ModelScope = new GameModelScope();
			SignalScope = new GameSignalScope();
			CommandScope = new GameCommandScope(this);	
			ManagerScope = new GameManagerScope(this);
			
			Configurator = new CoreConfigurator();
			
			ActionMediator = new GameActionMediator(this);
			ActionMediator.Initialize();
		}

		public void Release()
		{
			ActionMediator.Release();
		}
	}
}