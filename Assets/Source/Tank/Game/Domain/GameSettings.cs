﻿using System;
using System.Collections.Generic;
using Tank.Game.Data.Types;

namespace Tank.Game.Domain
{
	[Serializable]
	public class GameSettings
	{
		public TankData TankSettings;
		public List<EnemyData> EnemiesSettings;
	}
}