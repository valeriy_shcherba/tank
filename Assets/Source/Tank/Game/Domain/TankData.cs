﻿using System;

namespace Tank.Game.Domain
{
	[Serializable]
	public class TankData
	{
		public string Type;
		public int HP;
		public int MaxSpeedForward;
		public int MaxSpeedBack;
		public int RotationSpeed;
		public int AccelerationOfMovement;
		public int AccelerationOfDamping;

		public float Armor;
		
		public WeaponData[] Weapons;
	}
}