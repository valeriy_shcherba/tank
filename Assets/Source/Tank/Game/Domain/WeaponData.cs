﻿using System;
using Tank.Game.Data.Types;

namespace Tank.Game.Domain
{
	[Serializable]
	public class WeaponData
	{
		public WeaponType Type;
		public int Damage;
		public float AttackSpeed;
	}
}