﻿using System;
using Tank.Game.Data.Types;

namespace Tank.Game.Domain
{
    [Serializable]
    public class EnemyData
    {
        public EnemyType Type;
        public int Damage;
        public int MaxSpeed;
        public int Acceleration;
        public int HP;
        public float Armor;
    }
}