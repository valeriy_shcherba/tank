﻿using System.Collections.Generic;
using Tank.Game.Actors.Base;
using Tank.Game.Context;
using Tank.Game.Data.Types;

namespace Tank.Game.Mediators
{
	public class GameActionMediator : ContextInject
	{
		public GameActionMediator(GameContext context) : base(context)
		{
		}

		public void Initialize()
		{
			_tankDamageMadeSignal.AddListener(TankAttacked);
			_enemyDamageMadeSignal.AddListener(EnemyAttacked);
			_bulletExplodedSignal.AddListener(SetEnemyDamage);
			_gameFinishedSignal.AddListener(GameFinished);
		}

		public void Release()
		{
			_tankDamageMadeSignal.RemoveListener(TankAttacked);
			_enemyDamageMadeSignal.RemoveListener(EnemyAttacked);
			_bulletExplodedSignal.RemoveListener(SetEnemyDamage);
			_gameFinishedSignal.RemoveListener(GameFinished);
		}

		private void TankAttacked(WeaponType type)
		{
			_tankShootBulletCommand.Execute(type);
		}

		private void EnemyAttacked(int damage)
		{
			_enemyDamageMadeCommand.Execute(damage);
		}

		private void SetEnemyDamage(List<EnemyActor> enemies)
		{
			_tankDamageMadeCommand.Execute(enemies);
		}

		private void GameFinished()
		{
			_stopGameCommand.Execute();
		}
	}
}