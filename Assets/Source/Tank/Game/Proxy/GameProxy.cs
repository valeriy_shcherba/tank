﻿using Tank.Game.Context;
using Tank.Game.Data.Types;
using Tank.Game.Scopes;
using UnityEngine;

namespace Tank.Game.Proxy
{
    public class GameProxy : ContextInject
    {
        public GameSignalScope SignalScope => _context.SignalScope;

        public GameProxy(GameContext context) : base(context)
        {
            
        }

        public void InitializeGame()
        {
            _initializeGameCommand.Execute();   
        }

        public void StartGame()
        {
            _startGameCommand.Execute();
        }

        public void StartRocketBooster()
        {
            _startRocketCommand.Execute();
        }
    }
}