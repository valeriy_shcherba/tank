﻿using System.Collections.Generic;
using Tank.Game.Controllers.Tasks;
using UnityEngine;

namespace Tank.Game.Models
{
	public class TaskRegistryModel
	{
		private List<TaskBase> _entitiesList;

		public TaskRegistryModel()
		{
			_entitiesList = new List<TaskBase>();
		}

		public void RegisterTask(TaskBase task)
		{
			_entitiesList.Add(task);
		}

		public void UnregisterTask(TaskBase task)
		{
			_entitiesList.Remove(task);
		}

		public void Reset()
		{
			TaskBase[] tasks = _entitiesList.ToArray();
			
			for (int index = 0; index < tasks.Length; index++)
			{
				tasks[index].Stop(true);
			}
			
			_entitiesList = new List<TaskBase>();
		}

		public TaskBase[] GetAllRegistered()
		{
			return _entitiesList.ToArray();
		}
	}
}