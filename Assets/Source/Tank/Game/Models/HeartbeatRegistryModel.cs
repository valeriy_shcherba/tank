﻿using System.Collections.Generic;
using Tank.Game.Managers.Heartbeat;

namespace Tank.Game.Models
{
	public class HeartbeatRegistryModel
	{
		private List<IUpdatableEntity> _registeredEntities;
		
		public HeartbeatRegistryModel()
		{
			_registeredEntities = new List<IUpdatableEntity>();
		}

		public void RegisterEntity(IUpdatableEntity entity)
		{
			_registeredEntities.Add(entity);
		}

		public void UnregisterEntity(IUpdatableEntity entity)
		{
			_registeredEntities.Remove(entity);
		}

		public IUpdatableEntity[] GetAllRegisteredEntities()
		{
			return _registeredEntities.ToArray();
		}
	}
}