﻿using System.Collections.Generic;
using Tank.Game.Actors.Base;

namespace Tank.Game.Models
{
	public class EnemyRegistryModel
	{
		private List<EnemyActor> _rigisteredEnemiesList;
		
		public EnemyRegistryModel()
		{
			_rigisteredEnemiesList = new List<EnemyActor>();
		}

		public void RegisterEnemy(EnemyActor actor)
		{
			_rigisteredEnemiesList.Add(actor);
		}

		public void UnregisterEnemy(EnemyActor actor)
		{
			_rigisteredEnemiesList.Remove(actor);
		}

		public EnemyActor[] GetAllRegisteredEnemies()
		{
			return _rigisteredEnemiesList.ToArray();
		}

		public void Reset()
		{
			EnemyActor[] enemies = GetAllRegisteredEnemies();
			_rigisteredEnemiesList.Clear();
			
			for (int index = 0; index < enemies.Length; index++)
			{
				enemies[index].Despawn();
			}
		}
	}
}