﻿using Tank.Game.Context;
using Tank.Game.Data.Types;

namespace Tank.Game.Controllers.Tasks
{
	public class GameTask: TaskBase
	{
		public GameTask(GameContext gameContext): base(gameContext){}

		protected override void TickUpdate ()
		{
			if (_state == TaskState.STARTED)
			{
				float currentTime = _heartbeatManager.Time;
				float startTimeWithDelay = _startTime + _delay + (_stopPauseDelay - _startPauseDelay);

				if (currentTime < startTimeWithDelay) {
					return;
				}

				_startTime = currentTime;
	
				ChangeState (TaskState.RUNNING);
			} 

		}

		public virtual void Execute() {}
	}
}