﻿using Tank.Game.Context;
using Tank.Game.Data.Types;
using Tank.Game.Managers.Heartbeat;

namespace Tank.Game.Controllers.Tasks
{
	public delegate void TaskCallBack(TaskBase task);

	public class TaskBase: ContextInject, IUpdatableEntity
	{
		public string name;

		protected TaskState _state;

		protected TaskCallBack _initializeCallback;
		protected TaskCallBack _runCallback;
		protected TaskCallBack _startCallback;
		protected TaskCallBack _completeCallback;
		protected TaskCallBack _releaseCallback;

		protected float _delay;
		public float _startTime;
		protected float _startPauseDelay;
		protected float _stopPauseDelay;

		public TaskBase(GameContext gameContext): base(gameContext){}

		protected virtual void Initialize() 
		{
			_startTime = _heartbeatManager.Time;
			OnInitialized();
		}

		protected virtual void Release() 
		{
			_state = TaskState.NONE;

			_initializeCallback = null;
			_runCallback = null;
			_startCallback = null;
			_completeCallback = null;
			_releaseCallback = null;

			 _delay = 0f;
			_startTime = 0f;
		}

		protected virtual void Run() {}
		protected virtual void Complete() {}

		protected virtual void TickUpdate() { }

		protected virtual void ChangeState (TaskState state)
		{
			_state = state;
		
			switch (state)
			{
				case TaskState.STARTED:
				{
					_taskManager.AddTask(this);
					OnStarted();
					break;
				}

				case TaskState.RUNNING:
				{
					Run ();
					OnRun ();
					break;
				}

				case TaskState.STOPPED:
				{
					break;
				}

				case TaskState.COMPLETED:
				{
					_taskManager.RemoveTask(this);

					Complete ();
					OnCompleted ();
					break;
				}
			}
		}

		private void OnInitialized()
		{
			_initializeCallback?.Invoke (this);
		}

		private void OnStarted()
		{
			_startCallback?.Invoke (this);
		}

		private void OnCompleted()
		{
			_completeCallback?.Invoke (this);
		}

		private void OnRun()
		{
			_runCallback?.Invoke (this);
		}

		private void OnReleased()
		{
			_releaseCallback?.Invoke (this);
		}

		public virtual void Start()
		{
			Initialize();

			ChangeState(TaskState.STARTED);
		}
			
		public virtual void Stop(bool isCompleted) 
		{
			if (_state == TaskState.STOPPED || _state == TaskState.COMPLETED) return;
			
			ChangeState(TaskState.COMPLETED);

			OnReleased();
			Release();
		}

		public void UpdateEntity()
		{
			TickUpdate ();
		}

		public void SetOnInitialized(TaskCallBack callback)
		{
			_initializeCallback = callback;
		}

		public void SetOnRun(TaskCallBack callback)
		{
			_runCallback = callback;
		}

		public void SetOnStart(TaskCallBack callback)
		{
			_startCallback = callback;
		}

		public void SetOnCompleted(TaskCallBack callback)
		{
			_completeCallback = callback;
		}

		public void SetOnReleased(TaskCallBack callback)
		{
			_releaseCallback = callback;
		}

		public void SetDelay(float delay)
		{
			_delay = delay;
		}
	}
}
