﻿using DG.Tweening;
using Tank.Game.Actors.Base;
using Tank.Game.Context;
using UnityEngine;

namespace Tank.Game.Controllers.Tasks
{
	public class MoveCameraToEnemyTask : GameTask
	{
		public MoveCameraToEnemyTask(GameContext gameContext) : base(gameContext)
		{
		}

		private Transform _actorTransform;
		private Transform _cameraTransform;

		private Vector3 _cameraDefaultPosition;
		private Vector3 _cameraDefaultRotation;
		
		public void Setup(EnemyActor actor)
		{
			_actorTransform = actor.transform;
			
			_cameraTransform = _gameModel.MotionBlur.transform;
			
			_cameraDefaultPosition = _cameraTransform.position;
			_cameraDefaultRotation = _cameraTransform.eulerAngles;
		}

		protected override void Run()
		{
			base.Run();
				
			_actorTransform.DOMoveY(-20f, 2f).Play();

			Vector3 endCameraPosition = Vector3.Lerp(_cameraTransform.position, _actorTransform.position, 0.95f);
			
			Sequence s = DOTween.Sequence();
			s.Append(_cameraTransform.DOLookAt(_actorTransform.position, 2f));
			s.Append(_cameraTransform.DOMove(endCameraPosition, 1f));
			s.Append(_cameraTransform.DOMove(_cameraDefaultPosition, 1f).SetDelay(1f));
			s.Append(_cameraTransform.DORotate(_cameraDefaultRotation, 1f));
			s.OnStart(OnSequenceStarted);
			s.OnComplete(OnSequenceCompleted);
			s.Play();
		}

		private void OnSequenceStarted()
		{
			_gameModel.MotionBlur.enabled = true;
			_heartbeatManager.SetPause(true);
		}
		
		private void OnSequenceCompleted()
		{
			_gameModel.MotionBlur.enabled = false;
			_heartbeatManager.SetPause(false);
			
			Stop(true);
		}
	}
}