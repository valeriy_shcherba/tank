﻿using System.Collections.Generic;
using Tank.Game.Actors.Base;
using Tank.Game.Context;
using Tank.Game.Data.Types;
using Tank.Game.Domain;
using Tank.Game.Managers.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tank.Game.Controllers.Tasks
{
	public class GenerateEnemiesTask : GameTask
	{
		public GenerateEnemiesTask(GameContext gameContext) : base(gameContext)
		{
		}

		private List<Poolable> _poolabledEnemies;

		private const int MAX_ENEMIES = 10;
		private const float SPAWN_DELAY = 1f;

		private float _lastSpawnTime;
		
		protected override void Initialize()
		{
			_poolabledEnemies = new List<Poolable>();
		}

		protected override void Release()
		{
			_poolabledEnemies = null;
		}

		protected override void Run()
		{
			EnemyActor entity = null;
			
			for (int index = 0; index < MAX_ENEMIES; index++)
			{
				entity = SpawnEnemy();
			}
			
			PlayCameraAnimation(entity);
		}

		protected override void TickUpdate()
		{
			base.TickUpdate();

			if (_heartbeatManager.Time <= _lastSpawnTime + SPAWN_DELAY) return;
			
			if (_poolabledEnemies.Count >= MAX_ENEMIES) return;
			
			var entity = SpawnEnemy();
			
			PlayCameraAnimation(entity);

			_lastSpawnTime = _heartbeatManager.Time;
		}

		private EnemyActor SpawnEnemy()
		{
			int enemyKey = Random.Range(1, _configurator.EnemiesSettingsDictionary.Keys.Count + 1);
			EnemyType type = (EnemyType) enemyKey;
			
			EnemyData data = _configurator.EnemiesSettingsDictionary[type];
			Transform target = _gameModel.Tank.transform;
			
			var entity = _poolManager.SpawnEnemy<EnemyActor>(type);
			entity.Setup(data, target);
			entity.OnDespawned += OnEnemyDespawn;
			entity.transform.position = CalculateSpawnPosition(_gameModel.Tank);
			entity.transform.eulerAngles = Vector3.zero;
			
			_poolabledEnemies.Add(entity);
			_enemyModel.RegisterEnemy(entity);

			return entity;
		}
		
		private void OnEnemyDespawn(Poolable entity)
		{
			entity.OnDespawned -= OnEnemyDespawn;
				
			_poolabledEnemies.Remove(entity);
			_enemyModel.UnregisterEnemy((EnemyActor)entity);
		}

		private Vector3 CalculateSpawnPosition(GameActor tank)
		{
			Vector3 position = Vector3.zero;
			int directionX = tank.transform.position.x > 0 ? -1 : 1;
			int directionZ = tank.transform.position.z > 0 ? -1 : 1;

			position.x = Random.Range(1000f, 2000f) * directionX;
			position.y = 0;
			position.z = Random.Range(1000f, 2000f) * directionZ;
			
			return position;
		}

		private void PlayCameraAnimation(EnemyActor entity)
		{
			if (Random.Range(0, 100) >= 20) return;
			
			var cameraTask = new MoveCameraToEnemyTask(_context);
			cameraTask.Setup(entity);
			cameraTask.Start();
		}
	}
}