﻿using DG.Tweening;
using Tank.Game.Context;
using Tank.Game.Controllers.Tasks;
using UnityEngine;

namespace Tank.Game.Controllers.Commands
{
	public class StartGameCommand : Command
	{
		public StartGameCommand(GameContext context) : base(context)
		{
			
		}

		public override void Execute(params object[] objs)
		{
			_gameModel.Tank.Setup(_configurator.TankSettings);
			_gameModel.Tank.transform.position = Vector3.zero;
			_gameModel.Tank.transform.eulerAngles = Vector3.zero;
			
			_heartbeatManager.SetStart(true);
			DOTween.timeScale = 1f;
			
			var generateTask = new GenerateEnemiesTask(_context);
			generateTask.Start();
		}
	}
}