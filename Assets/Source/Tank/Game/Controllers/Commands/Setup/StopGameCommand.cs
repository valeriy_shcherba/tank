﻿using DG.Tweening;
using Tank.Game.Context;

namespace Tank.Game.Controllers.Commands
{
	public class StopGameCommand : Command
	{
		public StopGameCommand(GameContext context) : base(context)
		{	
		}

		public override void Execute(params object[] objs)
		{
			_heartbeatManager.SetStart(false);
			DOTween.timeScale = 0f;
			
			_enemyModel.Reset();
			_taskModel.Reset();
		}
	}
}