﻿using Tank.Game.Actors;
using Tank.Game.Context;
using Tank.Game.Data.Constants;
using Tank.Game.Data.Types;
using Tank.Game.Factory;
using Tank.Game.Root;
using Tank.Root;
using Tank.UI.Popups;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace Tank.Game.Controllers.Commands
{
    public class InitializeGameCommand : Command
    {
        public InitializeGameCommand(GameContext context) : base(context)
        {
        }

        public override void Execute(params object[] objs)
        {
            AppRoot.Instance.UIManager.ShowPopup<StartPopupController>();
            
            AppRoot.Instance.SoundManager.PlayMusic(SoundConstants.MUSIC_Background);
            AppRoot.Instance.SoundManager.PlayMusic(SoundConstants.SFX_TankEngine);
            
            TankFactory factory = new TankFactory();

            var tank = factory.GetInstance<TankActor>(TankType.TANK);
            tank.transform.SetParent(GameRoot.Instance.FieldTransform);
            tank.Setup(_configurator.TankSettings);
            tank.Spawn();

            _gameModel.Tank = tank;
            _gameModel.MotionBlur = Camera.main.GetComponent<MotionBlur>();
            
            _heartbeatManager.RegisterEntity(tank);
            _gameInitialized.Dispatch();
        }
    }
}