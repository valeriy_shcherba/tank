﻿using Tank.Game.Actors;
using Tank.Game.Context;
using Tank.Game.Data.Constants;
using Tank.Game.Data.Types;
using Tank.Root;

namespace Tank.Game.Controllers.Commands
{
	public class TankShootBulletCommand : Command
	{
		public TankShootBulletCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			AppRoot.Instance.SoundManager.PlaySfx(SoundConstants.SFX_Shoot);
			
			WeaponType weaponType = (WeaponType)objs[0];
			ProjectTileType bulletType = weaponType == WeaponType.TYPE_A ? ProjectTileType.TANK_BULLET_A
			 															 : ProjectTileType.TANK_BULLET_B;
			
			var bullet = _poolManager.SpawnProjectTile<GunBulletActor>(bulletType);
			bullet.transform.position = _gameModel.Tank.CurrentWeapon.SpawnPoint.position;
			bullet.transform.eulerAngles = _gameModel.Tank.CurrentWeapon.SpawnPoint.eulerAngles;
		}
	}
}