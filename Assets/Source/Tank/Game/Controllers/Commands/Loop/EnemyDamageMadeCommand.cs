﻿using Tank.Game.Context;

namespace Tank.Game.Controllers.Commands
{
	public class EnemyDamageMadeCommand : Command
	{
		public EnemyDamageMadeCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			int damage = (int) objs[0];
			
			_gameModel.Tank.SetDamage(damage);
		}
	}
}