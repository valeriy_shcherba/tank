﻿using Tank.Game.Actors;
using Tank.Game.Actors.Base;
using Tank.Game.Context;
using Tank.Game.Data.Constants;
using Tank.Game.Data.Types;
using Tank.Game.Managers.Pool;
using Tank.Root;
using UnityEngine;

namespace Tank.Game.Controllers.Commands
{
	public class StartRocketCommand : Command
	{
		public StartRocketCommand(GameContext context) : base(context)
		{
		}

		private bool _isSpawned;
		private RocketActor _rocket;
		
		public override void Execute(params object[] objs)
		{
			EnemyActor[] enemies = _enemyModel.GetAllRegisteredEnemies();

			if (_isSpawned || enemies.Length <= 0) return;
			
			EnemyActor enemy = enemies[0];
			float lastEnemyDistance = CalculateDistanceToTank(enemy);

			for (int index = 1; index < enemies.Length; index++)
			{
				float distance = CalculateDistanceToTank(enemies[index]);
				
				if (distance >= lastEnemyDistance) continue;

				enemy = enemies[index];
			}

			_isSpawned = true;
			
			AppRoot.Instance.SoundManager.PlaySfx(SoundConstants.SFX_Rocket);
			
			_rocket = _poolManager.SpawnProjectTile<RocketActor>(ProjectTileType.ROCKET);
			_rocket.transform.position = Vector3.up * 500f;
			_rocket.OnDespawned += OnRocketDespawned;
			_rocket.Setup(enemy);
		}

		private float CalculateDistanceToTank(EnemyActor enemy)
		{
			TankActor tank = _gameModel.Tank;
			return Vector3.Distance(enemy.transform.position, tank.transform.position);
		}

		private void OnRocketDespawned(Poolable entity)
		{
			_rocket.OnDespawned -= OnRocketDespawned;
			_isSpawned = false;
		}
	}
}