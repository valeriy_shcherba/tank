﻿using System.Collections.Generic;
using Tank.Game.Actors.Base;
using Tank.Game.Context;

namespace Tank.Game.Controllers.Commands
{
	public class TankDamageMadeCommand : Command
	{
		public TankDamageMadeCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			int damage = _gameModel.Tank.Damage;
			
			List<EnemyActor> enemies = (List<EnemyActor>) objs[0];
			enemies.ForEach(x => x?.SetDamage(damage));
		}
	}
}