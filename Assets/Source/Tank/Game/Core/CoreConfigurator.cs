﻿using System.Collections.Generic;
using System.Linq;
using Tank.Game.Data.Types;
using Tank.Game.Domain;
using UnityEngine;

namespace Tank.Game.Core
{
	public class CoreConfigurator
	{
		private const string GAME_SETTINGS_PATH = "Data/data_settings";

		public TankData TankSettings { get; }
		public Dictionary<EnemyType, EnemyData> EnemiesSettingsDictionary { get; }

		public CoreConfigurator()
		{	
			string gameConfig = Resources.Load<TextAsset>(GAME_SETTINGS_PATH).text;
			GameSettings gameSettings = JsonUtility.FromJson<GameSettings>(gameConfig);

			TankSettings = gameSettings.TankSettings;
			EnemiesSettingsDictionary = gameSettings.EnemiesSettings.ToDictionary(x => x.Type, x => x);
		}
	}
}