﻿namespace Tank.Game.Data.Constants
{
	public class SoundConstants
	{
		public static string MUSIC_Background = "Background";
		
		public static string SFX_EnemySpawn = "EnemySpawn";
		public static string SFX_EnemyDespawn = "EnemyDespawn";
		public static string SFX_Rocket = "Rocket";
		public static string SFX_Shoot = "Shoot";
		public static string SFX_TankEngine = "TankEngine";
	}
}