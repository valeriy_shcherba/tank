﻿using Common.Audio;
using Common.Singleton;
using Common.UI;
using Tank.Facade;
using Tank.Game.Root;
using Tank.UI.Windows;

namespace Tank.Root
{
	public class AppRoot : UnitySingleton<AppRoot>, IAppFacade
	{
		public SoundManager SoundManager { get; private set; }
		public UIManager UIManager { get; private set; }
		
		private void Start()
		{
			SoundManager = GetComponentInChildren<SoundManager>(true);
			
			UIManager = new UIManager();
			
			UIManager.Initialize();
			SoundManager.Initialize();
			
			GameRoot.Instance.Initialize();
			
			UIManager.ShowWindow<GameWindowController>();
		}
	}
}