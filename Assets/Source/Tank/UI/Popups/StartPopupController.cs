﻿using Common.UI;
using Tank.Game.Proxy;
using Tank.Game.Root;
using UnityEngine;

namespace Tank.UI.Popups
{
	[RequireComponent(typeof(StartPopupView))]
	public class StartPopupController : UIPopupController
	{
		[SerializeField] private StartPopupView _view;

		protected override void Initialize()
		{
			_view.StartButton.onClick.AddListener(OnStartButtonClick);
		}

		protected override void Release()
		{
			_view.StartButton.onClick.RemoveListener(OnStartButtonClick);
		}

		private void OnStartButtonClick()
		{
			GameProxy proxy = GameRoot.Instance.Proxy;
			
			proxy.StartGame();
			
			gameObject.SetActive(false);
		}
	}
}