﻿using UnityEngine;
using UnityEngine.UI;

namespace Tank.UI.Popups
{
	public class StartPopupView : MonoBehaviour
	{
		[SerializeField] private Button _startButton;
		
		public Button StartButton => _startButton;
	}
}