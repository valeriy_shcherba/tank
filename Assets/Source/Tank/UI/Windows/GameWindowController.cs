﻿using Common.UI;
using Tank.Game.Proxy;
using Tank.Game.Root;
using Tank.Root;
using Tank.UI.Popups;
using UnityEngine;

namespace Tank.UI.Windows
{
	[RequireComponent(typeof(GameWindowView))]
	public class GameWindowController : UIWindowController
	{
		[SerializeField] private GameWindowView _view;

		private GameProxy Proxy => GameRoot.Instance.Proxy;
		
		protected override void Initialize()
		{
			_view.RocketButton.onClick.AddListener(OnRocketButtonClicked);
			Proxy.SignalScope.GameFinishedSignal.AddListener(OnGameFinished);
		}

		protected override void Release()
		{
			_view.RocketButton.onClick.RemoveListener(OnRocketButtonClicked);
			Proxy.SignalScope.GameFinishedSignal.RemoveListener(OnGameFinished);
		}

		private void OnRocketButtonClicked()
		{
			GameProxy proxy = GameRoot.Instance.Proxy;
			proxy.StartRocketBooster();
		}

		private void OnGameFinished()
		{
			AppRoot.Instance.UIManager.ShowPopup<StartPopupController>();
		}
	}
}