﻿using UnityEngine;
using UnityEngine.UI;

namespace Tank.UI.Windows
{
	public class GameWindowView : MonoBehaviour
	{
		[SerializeField] private Button _rocketButton;

		public Button RocketButton => _rocketButton;
	}
}