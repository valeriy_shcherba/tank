﻿using Common.Audio;
using Common.UI;

namespace Tank.Facade
{
	public interface IAppFacade
	{
		SoundManager SoundManager { get; }
		UIManager UIManager { get; }
	}
}